import React, {Component} from 'react';
import PropTypes from 'prop-types';

// Redux
import {connect} from 'react-redux';
import {agregarCita} from '../actions/citasActions';
import { mostrarError } from '../actions/errorActions';
import uuid from 'uuid';

class AgregarCita extends Component {

    // refs de el form
    nombreMascotaRef = React.createRef();
    propietarioRef = React.createRef();
    fechaRef = React.createRef();
    horaRef = React.createRef();
    sintomasRef = React.createRef();

    // montamos las actions
    componentWillMount(){
        this.props.mostrarError(false);
    }

    // leer la cita
    crearNuevaCita = (e) => {
        e.preventDefault();

        const mascota = this.nombreMascotaRef.current.value,
              propietario =  this.propietarioRef.current.value,
              fecha =  this.fechaRef.current.value,
              hora =  this.horaRef.current.value,
              sintomas = this.sintomasRef.current.value

        // validamos formulario
        if(mascota === '' || propietario === '' || fecha === '' || hora === '' || sintomas === ''){
            this.props.mostrarError(true);
        } else {
            // crear el objeto
            const nuevaCita = {
                id:uuid(),
                mascota, 
                propietario, 
                fecha, 
                hora,
                sintomas, 
            }
            //console.log(nuevaCita);

            // nueva cita se viene a convertir en el payload en redux, osea datos de entrada o captura
            this.props.agregarCita(nuevaCita);
        }

        // reset al form (opcional)
        e.currentTarget.reset();

        // devolvemos el error a su original
        this.props.mostrarError(false);
    }

    render() {
        const existeError = this.props.error;
        //console.log(existeError)
        return(
        <div className="card mt-5">
                <div className="card-body">
                    <h2 className="card-title text-center mb-5">Agrega las citas aqui</h2>
                    <form onSubmit={this.crearNuevaCita}>
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Nombre Mascota</label>
                            <div className="col-sm-8 col-lg-10">
                                <input ref={this.nombreMascotaRef} type="text" className="form-control" placeholder="Nombre Mascota" />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Nombre Dueño</label>
                            <div className="col-sm-8 col-lg-10">
                                <input ref={this.propietarioRef} type="text" className="form-control"  placeholder="Nombre Dueño de la Mascota" />
                            </div>
                        </div>

                         <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Fecha</label>
                            <div className="col-sm-8 col-lg-4  mb-4 mb-lg-0">
                                <input ref={this.fechaRef} type="date" className="form-control" />
                            </div>                            

                            <label className="col-sm-4 col-lg-2 col-form-label">Hora</label>
                            <div className="col-sm-8 col-lg-4">
                                <input ref={this.horaRef} type="time" className="form-control" />
                            </div>
                        </div>
                        
                        <div className="form-group row">
                            <label className="col-sm-4 col-lg-2 col-form-label">Sintomas</label>
                            <div className="col-sm-8 col-lg-10">
                                <textarea ref={this.sintomasRef} className="form-control"></textarea>
                            </div>
                        </div>
                        <div className="form-group row justify-content-end">
                            <div className="col-sm-3">
                                <button type="submit" className="btn btn-success w-100">Agregar</button>
                            </div>
                        </div>
                    </form>
                    {existeError ? <div className="alert alert-danger text-center">Todos los campos son obligatorios</div> : ''}
                </div>
        </div>
        )
    }
}

AgregarCita.propTypes = {
    agregarCita : PropTypes.func.isRequired
}

// redux
// conectamos las acciones con el componente
const mapStateToProps = (state) => ({
    // es citas dos veces porque va primero a reducers/index.js y toma el state de ahi, y luego al citasreduceres.js
    citas: state.citas.citas,
    error: state.error.error
})

export default connect(mapStateToProps, {agregarCita, mostrarError}) (AgregarCita);