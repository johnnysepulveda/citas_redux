import React, {Component} from 'react';
import Cita from './Cita';
import PropTypes from 'prop-types';

// Redux
import { connect } from 'react-redux';
import { obtenerCitas } from '../actions/citasActions';

import store from '../store';

// Cuando se haya pasado un action o el state haya cambiado, 
// pedimos el state a redux, lo convertimos a JSON y lo montamos al localstorage
store.subscribe( () => {
    //console.log(store.getState() );
    localStorage.setItem('citas', JSON.stringify(store.getState() ));
})

class ListaCitas extends Component {

    componentDidMount(){
        this.props.obtenerCitas();
    }

    render() { 

        const citas = this.props.citas;
        //console.log(this.props)
        const mensaje = Object.keys(citas).length === 0 ? 'No hay citas' : 'Administra las citas aquí';
    
        return ( 
                <div className="card mt-5">
                    <div className="card-body">
                        <h2 className="card-title text-center">{mensaje}</h2>
                        <div className="lista-citas">
                                {Object.keys(this.props.citas).map(cita => (
                                    <Cita
                                        key={cita}
                                        info={this.props.citas[cita] }
                                        idCita={cita}
                                    />
                                ))}
                        </div>
                    </div>
                </div>
         )
    }
}

ListaCitas.propTypes  =  {
    citas : PropTypes.array.isRequired
}
 
//export default ListaCitas;

// redux
// conectamos las acciones con el componente
const mapStateToProps = state => ({
    // es citas dos veces porque va primero a reducers/index.js y toma el state de ahi, y luego al citasreduceres.js
    citas: state.citas.citas
})

export default connect(mapStateToProps, {obtenerCitas}) (ListaCitas);