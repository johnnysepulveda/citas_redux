// este archivo tiene todas las interacciones y la logica para cambiar el state

import { MOSTRAR_CITAS,AGREGAR_CITA,BORRAR_CITA } from '../actions/types';

// state inicial, cada reducer debe de tener su propio state
const initialState = {
    citas:[]
}

// EL REDUCER SIEMPRE RETORNA EL PROXIMO STATE

export default function(state = initialState, action){
    switch(action.type){
        case MOSTRAR_CITAS:
            return{
                ...state
            }
        case AGREGAR_CITA:
            return{
                ...state,
                // citas = {...this.state.citas};
                citas : [...state.citas, action.payload]  // tomamos una copia del state y le agregamos uno nuevo
            }
        case BORRAR_CITA:
            return{
                ...state,
                citas:  state.citas.filter(cita => cita.id !== action.payload)
            }
        default:
            return state;
    }
}