// EN ESTE ARCHIVO COMBINO TODOS LOS REDUCERS QUE NECESITE

import { combineReducers } from 'redux';

// importamos los reducers
import citasReducer from './citasReducer';
import errorReducer from './errorReducer';

export default combineReducers({

    // la action para el state citas es citasReducer
    citas: citasReducer,
    error: errorReducer
});