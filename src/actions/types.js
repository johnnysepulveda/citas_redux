// Los types describen que hacen mi aplicacion
// por ejemplo cuando apreto un boton se agrega una cita

export const MOSTRAR_CITAS = 'MOSTRAR_CITAS';
export const AGREGAR_CITA = 'AGREGAR_CITA';
export const BORRAR_CITA = 'BORRAR_CITA';

// validar formulario
export const VALIDAR_FORMULARIO = 'VALIDAR_FORMULARIO';
export const MOSTRAR_ERROR = 'MOSTRAR_ERROR';

