// aqui definimos las acciones que afectaran al state

import { BORRAR_CITA, AGREGAR_CITA, MOSTRAR_CITAS } from './types';

// este action se mapea con el reducers para hacer lo que este ahi
export const obtenerCitas = () => {
    return{
        type: MOSTRAR_CITAS
    }
}

export const agregarCita = (cita) => {
    return{
        type: AGREGAR_CITA,
        payload: cita
    }
}

export const borrarCita = (id) => {
    return{
        type: BORRAR_CITA,
        payload: id
    }
}