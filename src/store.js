// importamos redux 
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

// importar reducers
import rootReducer from './reducers';

//const initialState = {};

const middleware = [thunk];

// agregamos Local Storage
// en caso de que alla algo lo toma, sino crea un array vacio
const storageState = localStorage.getItem('citas') ? JSON.parse(localStorage.getItem('citas')) : [];

const store = createStore(rootReducer, storageState, compose(applyMiddleware(...middleware), 
    // Este codigo es para el devtools que instale en firefox
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
));

export default store;


